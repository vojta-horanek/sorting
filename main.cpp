#include <iostream>
#include <chrono>
#include <string>
#include <vector>
#include <random>
#include <algorithm>
#include <functional>
#include <iterator>
#include <cassert>
#include <thread>
#include <mutex>
#include <map>

#define TEST(msg, base, res) \
	for (auto i = 0; i < (base).size(); i++) \
		assert(((msg), (base)[i] == (res)[i]));

std::mutex output_mutex;
std::map<std::string, long int> durations;

class Timer {
	public:
		Timer(const std::string& timer_name) {
			name = timer_name;
			start = std::chrono::high_resolution_clock::now(); 
		}
		~Timer() {
			auto count = std::chrono::duration_cast<std::chrono::nanoseconds>((std::chrono::high_resolution_clock::now() - start)).count(); 
			std::lock_guard<std::mutex> guard(output_mutex);
			durations[name] = count;
		}
	private:
		std::string name;
		std::chrono::high_resolution_clock::time_point start;
};

template <typename T>
void print_vec(const std::vector<T>& vec) {
	for (auto& elem : vec) {
		std::cout << elem << " ";
	}
	std::cout << std::endl;
}

template <typename T>
void bubble_sort(std::vector<T> vec, std::vector<T>& res) {
	
	auto t = Timer("bubble");
	auto j = 0;
	for (auto i = 0; i < vec.size() - 1; i++) {
		for (j = 0; j < vec.size() - i - 1; j++) {
			if (vec[j] > vec[j+1]) {
				T tmp = vec[j];
				vec[j] = vec[j+1];
				vec[j+1] = tmp;
			}
		}
	}
	res = vec;
}

template <typename T>
void insertion_sort(std::vector<T> vec, std::vector<T>& res) {
	
	auto t = Timer("insertion");
	for (auto i = 1; i < vec.size(); i++) {
		T elem = vec[i];
		auto j = i;
		while (j > 0 && vec[j-1] > elem) {
			vec[j] = vec[j-1];
			j--; 
		}
		vec[j] = elem;
	}
	res = vec;
}

template <typename T>
void selection_sort(std::vector<T> vec, std::vector<T>& res) {

	auto t = Timer("selection");
	for (auto start = 0; start < vec.size() - 1; start++) {
		auto min = start;
		for (auto i = start + 1; i < vec.size(); i++) {
			if (vec[i] < vec[min]) {
				min = i;
			}
		}
		T tmp = vec[start];
		vec[start] = vec[min];
		vec[min] = tmp;
	}
	res = vec;
}

template <typename Iter1, typename Iter2>
void merge_sort_impl(Iter1 source_begin, Iter1 source_end,
							Iter2 target_begin, Iter2 target_end) {
	auto len = std::distance(source_begin, source_end);
	if (len < 2)
		return;

	auto left_len = len >> 1;
	auto source_left_end = source_begin;
	auto target_left_end = target_begin;

	std::advance(source_left_end, left_len);
	std::advance(target_left_end, left_len);

	merge_sort_impl(target_begin, target_left_end,
					source_begin, source_left_end);
	merge_sort_impl(target_left_end, target_end,
					source_left_end, source_end);

	std::merge(source_begin, source_left_end, source_left_end,
				source_end, target_begin);

}

template <typename Iter>
void _merge_sort(Iter begin, Iter end) {

	auto len = std::distance(begin, end);
	if (len < 2)
		return;
	using value_type = typename std::iterator_traits<Iter>::value_type;
	std::vector<value_type> vec(begin, end);
	merge_sort_impl(vec.begin(), vec.end(), begin, end);
}

template <typename T>
void merge_sort(std::vector<T> vec, std::vector<T>& res) {
	
	auto t = Timer("merge");
	_merge_sort(vec.begin(), vec.end());
	res = vec;
}

template <typename Iter>
auto q_partition(Iter begin, Iter end) {

	auto pivot = std::prev(end, 1);
	auto i = begin;
	for (auto j = begin; j != pivot; j++) {
		if (*j < *pivot) {
			std::iter_swap(i, j);
			std::advance(i, 1);
		}
	}
	std::iter_swap(i, pivot);
	return i;
}

template <typename Iter>
void quick_sort_impl(Iter begin, Iter end) {

	if (std::distance(begin, end) > 1) {
		auto middle = q_partition(begin, end);
		quick_sort_impl(begin, middle);
		quick_sort_impl(middle + 1, end);
	}
}

template <typename T>
void quick_sort(std::vector<T> vec, std::vector<T>& res) {
	
	auto t = Timer("quick");
	quick_sort_impl(vec.begin(), vec.end());
	res = vec;
}

void sort(size_t elem_count) {

	std::random_device rnd_dev;
	std::mt19937 engine {rnd_dev()};

	std::vector<int> vec(elem_count); 
	std::iota(vec.begin(), vec.end(), 1);

	std::shuffle(vec.begin(), vec.end(), engine);

	std::vector<int> vec_bub, vec_ins, vec_sel, vec_mer, vec_qui;

	std::thread t[] {
		std::thread(bubble_sort<int>, vec, std::ref(vec_bub)),
		std::thread(insertion_sort<int>, vec, std::ref(vec_ins)),
		std::thread(selection_sort<int>, vec, std::ref(vec_sel)),
		std::thread(merge_sort<int>, vec, std::ref(vec_mer)),
		std::thread(quick_sort<int>, vec, std::ref(vec_qui))
	};

	for (auto& thread : t) {
		thread.join();
	}

	//sort start vector for test
	std::sort(vec.begin(), vec.end());

	// Run tests
	TEST("bubble", vec, vec_bub);
	TEST("insertion", vec, vec_ins);
	TEST("selection", vec, vec_sel);
	TEST("merge", vec, vec_mer);
	TEST("quick", vec, vec_qui);

	std::cout << elem_count << "," << 
		durations["bubble"] << "," <<
		durations["insertion"] << "," <<
		durations["selection"] << "," << 
		durations["merge"] << "," << 
		durations["quick"] << std::endl;
}

int main(int argc, char* argv[]) {

	if (argc < 2) return 1;
	int elem_count = 0;
	elem_count = atoi(argv[1]);
	if (elem_count == 0) return 1;
	
	int repeat = 1;
	if (argc == 3) repeat = atoi(argv[2]);
	if (repeat == 0) return 0;

	std::cout << "count,bubble,insertion,selection,merge,quick" << std::endl;
	for (int i = 0; i < repeat; i++) {
		sort(elem_count);
	}

	return 0;
}
